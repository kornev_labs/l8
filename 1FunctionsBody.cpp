#include <iostream>
#include "FunctionsCaption.hpp"
void ReadLen(int& length)
{
	std::cout << "How many numbers will be in the sequence?" << std::endl;
	std::cin >> length;
	while (length > 1000)
	{
		std::cout << "The amount of numbers must not exceed 1000. Please try again" << std::endl;
		std::cin >> length;
	}
}
void ReadSeq(int* sequence, int& length)
{
	std::cout << "Enetr sequence numbers" << std::endl;
	for (int i = 0; i < length; i++)
	{
		std::cin >> sequence[i];
	}
}
int SumOfDigit(int* sequence, int ind)
{
	int temp = sequence[ind];
	int sum = 0;
	while (temp > 0)
	{
		sum += temp % 10;
		temp /= 10;
	}
	return sum;

}
int FirstDigit(int* sequence, int ind)
{
	int temp = sequence[ind];
	int firstDigit;
	while (temp > 0)
	{
		firstDigit = temp % 10;
		temp /= 10;
	}
	return firstDigit;
}
void Sort(int* sequence, int& length, int& ind)
{
	for (int i = 0; i < length; i++)
	{
		for (int x = i + 1; x < length; x++)
		{
			if (SumOfDigit(sequence, i) > SumOfDigit(sequence, x))
			{
				std::swap(sequence[i], sequence[x]);
			}
			else if (SumOfDigit(sequence, i) == SumOfDigit(sequence, x))
			{
				if (FirstDigit(sequence, i) > FirstDigit(sequence, x))
				{
					std::swap(sequence[i], sequence[x]);
				}
				else if (FirstDigit(sequence, i) == FirstDigit(sequence, x))
				{
					if (sequence[i] > sequence[x])
					{
						std::swap(sequence[i], sequence[x]);
					}
				}
			}
		}
	}

}
void Write(int* sequence, int& length)
{
	std::cout << "Program finished sucssesfully. Processed sequence: " << std::endl;
	for (int i = 0; i < length; i++)
	{
		std::cout << sequence[i] << " ";
	}
}
