#pragma once
#include <iostream>
void ReadLen(int& length);
void ReadSeq(int* sequence, int& length);
int SumOfDigit(int* sequence, int ind);
int FirstDigit(int* sequence, int ind);
void Sort(int* sequence, int& length, int& ind);
void Write(int* sequence, int& length);
