/*1.Ввести последовательность натуральных чисел {Aj}j=1...n (n<=1000). Упорядочить последовательность по неубыванию суммы цифр числа, 
числа с одинаковыми суммами цифр дополнительно упорядочить по неубыванию первой цифры числа, числа с одинаковыми суммами цифр и одинаковыми первыми цифрами 
дополнительно упорядочить по неубыванию самого числа.*/
#include <iostream>
#include "FunctionsCaption.hpp"
int main()
{
	//переменная, для передавания индекса числа в функции
	int ind;
	int length;
	ReadLen(length);
	int* sequence = new int[length];
	ReadSeq(sequence, length);
	Sort(sequence, length, ind);
	Write(sequence, length);
	delete[] sequence;
	return 0;
}
