﻿/*2.Дана целочисленная матрица {Aij}i=1..n,j=1..m (n,m<=100). 
Найти строку с наибольшей по абсолютной величине суммой элементов и заменить все элементы этой строки числом 9999.*/
#include <iostream>
#include "FunctionsCaption2.hpp"
int main()
{
	int I;
	int J;
	Read(I, J);
	int** matrix = new int* [I];
	for (int i = 0; i < I; i++)
	{
		matrix[i] = new int[J];
	}
	ReadMatrix(matrix, I, J);
	int ind = FindMaxLine(matrix, I, J);
	ReplaceElements(matrix, J, ind);
	Write(matrix, I, J, ind);
	for (int i = 0; i < I; i++)
	{
		delete matrix[i];
	}
	delete[] matrix;
	return 0;
}


