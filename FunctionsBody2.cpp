#include <iostream>
#include "FunctionsCaption2.hpp"
void Read(int& I, int& J)
{
	std::cout << "How many lines will be in the matrix?" << std::endl;
	std::cin >> I;
	while (I > 100)
	{
		std::cout << "The count of lines must not exceed 100. Try again" << std::endl;
		std::cin >> I;
	}
	std::cout << "How many columns will be in the matrix?" << std::endl;
	std::cin >> J;
	while (J > 100)
	{
		std::cout << "The count of columns must not exceed 100. Try again" << std::endl;
		std::cin >> J;
	}
}
int FindMaxLine(int** matrix, int& I, int& J)
{
	int indMaxLine;
	int maxSumLine = INT_MIN;
	for (int i = 0; i < I; i++)
	{
		int sumLine = 0;
		for (int x = 0; x < J; x++)
		{
			sumLine += matrix[i][x];
		}
		if (sumLine > maxSumLine)
		{
			maxSumLine = sumLine;
			indMaxLine = i;
		}

	}
	return indMaxLine;
}
void ReadMatrix(int** matrix, int& I, int& J)
{
	for (int i = 0; i < I; i++)
	{
		std::cout << "Enter " << i+1 << " line" << std::endl;
		for (int x = 0; x < J; x++) 
		{
			std::cin >> matrix[i][x];
		}
	}
}
void ReplaceElements(int** matrix, int& J, int& ind)
{
	for (int x = 0; x < J; x++)
	{
		matrix[ind][x] = 9999;
	}
}
void Write(int** matrix, int& I, int& J, int& ind)
{
	std::cout << "The biggest line is " << ind + 1 << std::endl;
	std::cout << "The complete matrix is " << std::endl;
	for (int i = 0; i < I; i++)
	{
		for (int x = 0; x < J; x++)
		{
			std::cout << matrix[i][x] << " ";
		}
		std::cout << std::endl;
	}
}