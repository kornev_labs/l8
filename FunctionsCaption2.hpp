#pragma once
#include <iostream>
void Read(int& I, int& J);
void ReadMatrix(int** matrix, int& I, int& J);
int FindMaxLine(int** matrix, int& I, int& J);
void ReplaceElements(int** matrix, int& J, int& ind);
void Write(int** matrix, int& I, int& J, int&ind);
